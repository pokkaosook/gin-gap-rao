package com.example.GinGapRao.ui.orders

import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.GinGapRao.R
import com.example.GinGapRao.data.model.CollectionOrderModel
import com.example.GinGapRao.ui.orders.adapter.OrdersMainTabAdapter
import com.example.GinGapRao.ui.orders.detail.OrderItemDetailFragment
import com.example.GinGapRao.ui.orders.detail.OrderItemReadyToPayDetailFragment
import com.example.GinGapRao.ui.orders.orders.OrderListFragment
import com.example.GinGapRao.ui.orders.payments.OrderReadyToPayFragment
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_orders_main.*

class OrdersMainFragment : Fragment(), OrderListFragment.OnItemOrderClickListener,
    OrderReadyToPayFragment.OnItemOrderReadyToPayClickListener {

    private lateinit var orderListFragment: Fragment
    private lateinit var orderReadyToPayFragment: Fragment

    private var mOrderItemDetailFragment: OrderItemDetailFragment? = null
    private var mOrderItemReadyToPayDetailFragment: OrderItemReadyToPayDetailFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        orderListFragment = OrderListFragment.newInstance()
        orderReadyToPayFragment = OrderReadyToPayFragment.newInstance()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_orders_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {
        viewPagerOrders.apply {
            adapter = OrdersMainTabAdapter(childFragmentManager,
                listOf(orderListFragment, orderReadyToPayFragment))
        }

        viewPagerOrders?.let {
            tabLayoutOrders?.setupWithViewPager(it)
        }

        viewPagerOrders.offscreenPageLimit = 2
//        tabLayoutOrders.applyFont(Typeface.create("sans-serif-light", Typeface.NORMAL));

        tabLayoutOrders.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                val currentTab = tab?.position
                if(currentTab == 0){
                    onRemoveFragmentTab2()
                }else {
                    onRemoveFragmentTab1()
                }
            }
            override fun onTabReselected(tab: TabLayout.Tab?) {
                // Do nothing
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                // Do nothing
            }
        })
    }

    private fun TabLayout.applyFont(typeface: Typeface) {
        val viewGroup = getChildAt(0) as ViewGroup
        val tabsCount = viewGroup.childCount
        for (j in 0 until tabsCount) {
            val viewGroupChildAt = viewGroup.getChildAt(j) as ViewGroup
            val tabChildCount = viewGroupChildAt.childCount
            for (i in 0 until tabChildCount) {
                val tabViewChild = viewGroupChildAt.getChildAt(i)
                if (tabViewChild is TextView) {
                    tabViewChild.typeface = typeface
                }
            }
        }
    }

    fun onRemoveFragmentTab1() {
        activity?.supportFragmentManager?.let {
            this.mOrderItemDetailFragment?.let { it1 ->
                it.beginTransaction()
                    .remove(it1)
                    .commit()
            }
        }
    }

    fun onRemoveFragmentTab2() {
        activity?.supportFragmentManager?.let {
            this.mOrderItemReadyToPayDetailFragment?.let { it1 ->
                it.beginTransaction()
                    .remove(it1)
                    .commit()
            }
        }
    }

    override fun onItemOrderClick(data: CollectionOrderModel) {
        mOrderItemDetailFragment = OrderItemDetailFragment.newInstance(data)
        (activity as OrdersMainActivity).replaceFragmentToContentContainer(orderDetailContainer.id,
            mOrderItemDetailFragment!!,
            OrderItemDetailFragment.TAG_NAME)
    }

    override fun onItemReadyToPayClick(data: CollectionOrderModel) {
        mOrderItemReadyToPayDetailFragment = OrderItemReadyToPayDetailFragment.newInstance(data)
        (activity as OrdersMainActivity).replaceFragmentToContentContainer(orderDetailContainer.id,
            mOrderItemReadyToPayDetailFragment!!,
            OrderItemReadyToPayDetailFragment.TAG_NAME)
    }

    companion object {
        const val TAG_NAME = "OrdersMainFragment"
    }
}
