package com.example.GinGapRao.ui.orders.detail

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.GinGapRao.R
import com.example.GinGapRao.data.model.CollectionOrderModel
import com.example.GinGapRao.extensions.lazyFast
import com.example.GinGapRao.ui.orders.detail.adapter.OrderItemDetailAdapter
import com.example.GinGapRao.ui.orders.payments.PaymentViaQRFragment
import com.example.GinGapRao.utils.dialog.DialogConfirmGetSlip
import com.example.GinGapRao.utils.dialog.DialogSuccess
import com.example.GinGapRao.viewmodel.OrdersReadyToPayViewModel
import kotlinx.android.synthetic.main.fragment_order_ready_to_pay_detail.*

class OrderItemReadyToPayDetailFragment : Fragment(), PaymentViaQRFragment.OnItemOrderPaidClickListener {

    private val mDetailOrderAdapter by lazyFast { OrderItemDetailAdapter() }
    private var order: CollectionOrderModel? = null

    private lateinit var ordersReadyToPayViewModel: OrdersReadyToPayViewModel
    private lateinit var viewModelFactory: OrdersReadyToPayViewModel.OrderViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            order = arguments?.getParcelable(EXTRA_READY_PAY_CODE)
        }

        initViewModel()
    }

    private fun initViewModel() {
        viewModelFactory = OrdersReadyToPayViewModel.OrderViewModelFactory()
        ordersReadyToPayViewModel =
            ViewModelProvider(
                this@OrderItemReadyToPayDetailFragment,
                viewModelFactory
            )
                .get(OrdersReadyToPayViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_order_ready_to_pay_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        order?.let { initView(it) }

        initListener()
    }

    private fun initListener() {
        btnPayViaQR.setOnClickListener {
            order?.let { it ->
                openOrderReadyToPayFragment()
            }
        }

        btnPayViaCash.setOnClickListener {
            DialogConfirmGetSlip(context as Context).apply {
                positiveClick {
                    // Print Slip
                    ordersReadyToPayViewModel.orderPrinter(order)
                    ordersReadyToPayViewModel.updateStatusDetailItemOrder(order)
                    DialogSuccess(context as Context).create().show()
                    removeThisFragment()
                }
                negativeClick {
                    // Done and Update Status Done
                    ordersReadyToPayViewModel.updateStatusDetailItemOrder(order)
                    DialogSuccess(context as Context).create().show()
                    removeThisFragment()
                }
                cancelClick {
                    dialog?.dismiss()
                }
            }.create().show()
        }
    }

    private fun removeThisFragment(){
        activity?.supportFragmentManager?.let {
            this@OrderItemReadyToPayDetailFragment.let { it1 ->
                it.beginTransaction()
                    .remove(it1)
                    .commit()
            }
        }

    }

    private fun initView(it: CollectionOrderModel) {
        txtReadyPayDetailNumberTable.text = order?.tableNo ?: ""
        txtReadyPayItemAmountOrder.text = "จำนวนทั้งหมด ${order?.items?.size} รายการ"
        txtReadyPayNumberOrder.text = "สถานะทำรายการ : พร้อมชำระเงิน"

        var totalPrice = 0
        it.items?.forEachIndexed { _, orderDetail ->
            totalPrice = (totalPrice + orderDetail.price!!).toInt()
        }
        textTotalPriceOfBill.text = "$totalPrice"

        it.items?.forEachIndexed { _, orderDetail ->
            orderDetail.apply {
                orderStatus = it.status
                foodStatus = orderDetail.foodStatus
                description = orderDetail.description
                name = orderDetail.name
                price = orderDetail.price
                image = orderDetail.image
                qty = orderDetail.qty
            }
        }

        mDetailOrderAdapter.addAllAndRefresh(it.items!!)
        rvReadyToPayOrdersDetail.apply {
            layoutManager = LinearLayoutManager(context as Context)
            setHasFixedSize(true)
            adapter = mDetailOrderAdapter
        }
    }

    private fun openOrderReadyToPayFragment(){
        activity?.supportFragmentManager?.let {
            val dialogFragment = PaymentViaQRFragment.newInstance(order)
            dialogFragment.setListener(this)
            dialogFragment.show(it, PaymentViaQRFragment.TAG_NAME)
        }
    }

    override fun onItemPaidClick() {
        DialogSuccess(context as Context).create().show()

        activity?.supportFragmentManager?.let {
            it.beginTransaction()
                .remove(this@OrderItemReadyToPayDetailFragment)
                .commit()
        }
    }

    companion object {
        const val TAG_NAME = "OrderItemReadyToPayDetailFragment"
        const val EXTRA_READY_PAY_CODE = "EXTRA_READY_PAY_CODE"

        fun newInstance(data: CollectionOrderModel?= null) = OrderItemReadyToPayDetailFragment()
            .apply {
                arguments = Bundle().apply {
                    putParcelable(EXTRA_READY_PAY_CODE, data)
                }
            }
    }

}
