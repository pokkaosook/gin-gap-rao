package com.example.GinGapRao.ui.main

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.example.GinGapRao.R
import com.example.GinGapRao.data.model.MainMenuItemModel
import com.example.GinGapRao.extensions.lazyFast
import com.example.GinGapRao.extensions.openActivity
import com.example.GinGapRao.printer.HardwareHelper
import com.example.GinGapRao.printer.PrinterHelper
import com.example.GinGapRao.ui.main.adapter.GinGapRaoMainAdapter
import com.example.GinGapRao.ui.orders.OrdersMainActivity
import com.example.GinGapRao.ui.tablemnt.TableManagementActivity
import kotlinx.android.synthetic.main.fragment_gingaprao_main_menu.*

class GinGapRaoMainFragment : Fragment() {

    private val mMainAdapter by lazyFast { GinGapRaoMainAdapter() }
    private lateinit var printer : PrinterHelper

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_gingaprao_main_menu, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initListener()
    }


    private fun initView() {

        printer = PrinterHelper(object : PrinterHelper.PrinterListener {
            override fun onStartPrint() {}

            override fun onPrintFinished(success: Boolean) {}

            override fun onError(errorCode: PrinterHelper.PrinterErrorCode) {}

        })

        try {
            val hardwareHelper = HardwareHelper()
            hardwareHelper.isDeviceSupportedPrintFeature()
            val havePaper = printer.isHavePaper()
            if(havePaper){
            mMainAdapter.addAllAndRefresh(
                listOf(MainMenuItemModel(MANAGER),
                    MainMenuItemModel(ORDERS),
                    MainMenuItemModel(PRINTER))
            ) } else {
                mMainAdapter.addAllAndRefresh(
                    listOf(MainMenuItemModel(MANAGER),
                        MainMenuItemModel(ORDERS))
                )
            }
        } catch (e: java.lang.Exception){
            e.printStackTrace()
            mMainAdapter.addAllAndRefresh(
                listOf(MainMenuItemModel(MANAGER),
                    MainMenuItemModel(ORDERS))
            )
        }


        listItemMainMenu.apply {
            layoutManager = GridLayoutManager(context, 2)
            setHasFixedSize(true)
            adapter = mMainAdapter
        }
    }

    private fun initListener() {
        mMainAdapter.setListener(object : GinGapRaoMainAdapter.ItemMainMenuListener {
            override fun onItemClick(data: MainMenuItemModel) {
                when(data.entryItemName){
                    MANAGER -> {
                        openActivity<TableManagementActivity>()
                    }
                    ORDERS -> {
                        openActivity<OrdersMainActivity>()
                    }
                    PRINTER -> {
//                        openActivity<Printer>()
                    }
                }
            }


        })
    }

    companion object {
        const val TAG_NAME = "GinGapRaoMainFragment"
        const val MANAGER = "Manager"
        const val ORDERS = "Orders"
        const val PRINTER = "Printer"
    }

}