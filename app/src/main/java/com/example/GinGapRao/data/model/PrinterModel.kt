package com.example.GinGapRao.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PrinterModel(
    var orderId : String? = null,
    var shopId : String? = null,
    var status : String? = null,
    var tableNo : String? = null
) : Parcelable