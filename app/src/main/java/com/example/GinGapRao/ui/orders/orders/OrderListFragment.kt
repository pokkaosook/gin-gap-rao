package com.example.GinGapRao.ui.orders.orders

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.GinGapRao.R
import com.example.GinGapRao.data.model.CollectionOrderModel
import com.example.GinGapRao.extensions.*
import com.example.GinGapRao.ui.orders.orders.adapter.OrderListAdapter
import com.example.GinGapRao.viewmodel.OrdersMainViewModel
import kotlinx.android.synthetic.main.fragment_order_list.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class OrderListFragment : Fragment() {

    private val mOrderAdapter by lazyFast { OrderListAdapter() }
    private val viewModel: OrdersMainViewModel by viewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_order_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeDataOrders()
        initListener()

        viewModel.getOrderFireStore()

    }

    private fun observeDataOrders() {
        observe(viewModel.orderList){
            if (it != null && it.isNotEmpty()) {
                initView(it)
                listOrders.visible()
                contWaitingOrders.gone()
            } else {
                listOrders.gone()
                contWaitingOrders.visible()
            }
        }
    }

    private fun initView(it: List<CollectionOrderModel>) {
        mOrderAdapter.addAllAndRefresh(it)

        listOrders.apply {
            layoutManager = LinearLayoutManager(context as Context)
            setHasFixedSize(true)
            adapter = mOrderAdapter
        }
    }

    private fun initListener() {
        mOrderAdapter.setListener(object : OrderListAdapter.ItemOrderListener {
            override fun onItemClick(data: CollectionOrderModel) {
                delegateOnFinishButtonClick(data)
            }

        })
    }

    private fun delegateOnFinishButtonClick(data: CollectionOrderModel) {
        delegateTo<OnItemOrderClickListener> {
            it.onItemOrderClick(data)
        }
    }

    interface OnItemOrderClickListener {
        fun onItemOrderClick(data: CollectionOrderModel)
    }

    companion object {


        @JvmStatic
        fun newInstance() = OrderListFragment()
    }

}