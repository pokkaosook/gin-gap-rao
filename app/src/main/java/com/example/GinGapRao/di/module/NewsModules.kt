package com.example.GinGapRao.di.module

import com.example.GinGapRao.data.repository.NewsRepository
import com.example.GinGapRao.data.repository.NewsRepositoryImpl
import com.example.GinGapRao.data.repository.TableRepository
import com.example.GinGapRao.data.repository.TableRepositoryImpl
import com.example.GinGapRao.domain.usecase.GetNewsListUseCase
import com.example.GinGapRao.viewmodel.NewsViewModel
import com.example.GinGapRao.viewmodel.OrdersMainViewModel
import com.example.GinGapRao.viewmodel.OrdersReadyToPayViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val newsModule = module {

    factory<NewsRepository> {
        NewsRepositoryImpl(
            get()
        )
    }

    factory<TableRepository> {
        TableRepositoryImpl()
    }

    viewModel { NewsViewModel(get()) }

    single { GetNewsListUseCase(get()) }

    viewModel { OrdersMainViewModel() }

    viewModel { OrdersReadyToPayViewModel() }
}
