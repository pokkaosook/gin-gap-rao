package com.example.GinGapRao.ui.orders.detail.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.GinGapRao.data.model.OrderDetail

class OrderItemDetailAdapter : RecyclerView.Adapter<OrderItemDetailViewHolder>() {

    val listMainMenu by lazy { mutableListOf<OrderDetail>() }

    private var listener: ItemDetailOrderListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderItemDetailViewHolder {
        return OrderItemDetailViewHolder.create(parent)
    }

    override fun getItemCount(): Int {
        return listMainMenu.size
    }

    override fun onBindViewHolder(holder: OrderItemDetailViewHolder, position: Int) {
        return holder.bind(position, listMainMenu[position], listener)
    }

    fun addAllAndRefresh(listMenu: List<OrderDetail>) {
        if(listMainMenu.size > 0) listMainMenu.clear()
        listMainMenu.addAll(listMenu)
        notifyDataSetChanged()
    }

    fun setListener(listener: ItemDetailOrderListener) {
        this.listener = listener
    }

    interface ItemDetailOrderListener {
        fun onItemClickCheckBox(data: OrderDetail)
    }

}