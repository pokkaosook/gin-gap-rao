package com.example.GinGapRao.ui.orders

import android.os.Bundle
import com.example.GinGapRao.R
import com.example.GinGapRao.ui.base.BaseToolbarActivity

class OrdersMainActivity : BaseToolbarActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        toolbar.title = getString(R.string.title_header_orders)

        loadContentFragmentInfo()
    }

    private fun loadContentFragmentInfo() {
        replaceFragmentToContentContainer(OrdersMainFragment(), OrdersMainFragment.TAG_NAME)
    }

}
