package com.example.GinGapRao.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.GinGapRao.data.model.CollectionOrderModel
import com.example.GinGapRao.data.model.OrderDetail
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase

class OrdersMainViewModel() : ViewModel()  {

    val orderList = MutableLiveData<List<CollectionOrderModel>>()
    val orderStatusDone = MutableLiveData<List<CollectionOrderModel>>()
    val currentOrder = MutableLiveData<CollectionOrderModel>()

    fun getOrderFireStore(){

        val db = Firebase.firestore

        db.collection("orders")
            .whereIn("status", mutableListOf("NONE", "IN_PROGRESS", "DONE"))
            .addSnapshotListener { snapshot, error ->
                if (snapshot != null && !snapshot.isEmpty) {

                    val list = arrayListOf<CollectionOrderModel>()
                    snapshot.documents.forEachIndexed{index, documentSnapshot ->
                        val temp = documentSnapshot.toObject<CollectionOrderModel>()
                        temp?.id = documentSnapshot.id
                        if (temp != null) {
                            list.add(temp)
                        }
                    }

                    orderList.value = list.toList()

                } else {
                    val taskList: List<CollectionOrderModel> = listOf()
                    orderList.value = taskList
                }


                if (error != null) {
                    return@addSnapshotListener
                }
            }
    }

    fun getOrderStatusDoneOnFireStore(){
        Firebase.firestore.collection("orders")
            .whereEqualTo("status","DONE")
            .addSnapshotListener { snapshot, error ->
                if (snapshot != null && !snapshot.isEmpty) {
//                    for (document in snapshot) {
//                        Log.i("Log111111","${document.data}")
//                    }
                    val taskList: List<CollectionOrderModel> = snapshot.toObjects(CollectionOrderModel::class.java)

                    orderStatusDone.value = taskList

                } else {
                    val taskList: List<CollectionOrderModel> = listOf()
                    orderStatusDone.value = taskList
                }

                if (error != null) {
                    return@addSnapshotListener
                }
            }
    }

    fun updateStatusDetailItemOrder(
        document: String,
        data: MutableList<OrderDetail>
    ){
        val db = Firebase.firestore
        db.collection("orders").document(document)
            .update("items", data)

    }

    fun updateStatusOrder(document: String, statusOrder: String) {  //"PENDING"
        val db = Firebase.firestore
        db.collection("orders").document(document)
            .update(
                hashMapOf(
                    "status" to statusOrder
                ) as Map<String, String>
            ).addOnCompleteListener {
                val temp = currentOrder.value
                temp?.status = statusOrder
                currentOrder.value = temp
            }
    }

    fun setCurrentOrder(order: CollectionOrderModel?) {
        currentOrder.value = order
    }

}