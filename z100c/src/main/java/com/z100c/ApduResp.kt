package com.z100c

import vpos.apipackage.APDU_RESP

class ApduResp(resp: ByteArray) {
    private val apduResp = APDU_RESP(resp)

    fun getLenOut(): Short {
        return apduResp.LenOut
    }

    fun getDataOut(): ByteArray? {
        return apduResp.DataOut
    }

    fun getSWA(): Byte {
        return apduResp.SWA
    }

    fun getSWB(): Byte {
        return apduResp.SWB
    }
}