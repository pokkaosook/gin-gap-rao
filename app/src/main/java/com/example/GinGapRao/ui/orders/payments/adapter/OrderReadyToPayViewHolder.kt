package com.example.GinGapRao.ui.orders.payments.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.GinGapRao.R
import com.example.GinGapRao.data.model.CollectionOrderModel
import com.example.GinGapRao.extensions.gone
import com.example.GinGapRao.extensions.inflate
import com.example.GinGapRao.extensions.visible
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.adapter_view_item_ready_to_pay.view.*

class OrderReadyToPayViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
    LayoutContainer {

    fun bind(data: CollectionOrderModel, listener: OrderReadyToPayAdapter.OrderReadyToPayListener?) {
        data.let {

            containerView.txtReadyPayNumberTable.text = it.tableNo ?: ""
            containerView.txtCountItemOrder.text = "${it.items?.size} รายการ"

            when {
                it.items!!.size > 3 -> {
                    containerView.txtReadyPayFirstItemOrder.visible()
                    containerView.txtReadyPaySecondItemOrder.visible()
                    containerView.txtReadyPayThirdItemOrder.visible()

                    containerView.txtReadyPayFirstItemOrder.text = "${it.items[0].name} X${it.items[0].qty}"
                    containerView.txtReadyPaySecondItemOrder.text = "${it.items[1].name} X${it.items[1].qty}"
                    containerView.txtReadyPayThirdItemOrder.text = "${it.items[2].name} X${it.items[2].qty}"

                }
                it.items.size == 2 -> {
                    containerView.txtReadyPayFirstItemOrder.visible()
                    containerView.txtReadyPaySecondItemOrder.visible()
                    containerView.txtReadyPayThirdItemOrder.gone()

                    containerView.txtReadyPayFirstItemOrder.text = "${it.items[0].name} X${it.items[0].qty}"
                    containerView.txtReadyPaySecondItemOrder.text = "${it.items[1].name} X${it.items[1].qty}"
                }
                else -> {
                    containerView.txtReadyPayFirstItemOrder.visible()
                    containerView.txtReadyPaySecondItemOrder.gone()
                    containerView.txtReadyPayThirdItemOrder.gone()

                    containerView.txtReadyPayFirstItemOrder.text = "${it.items[0].name} X${it.items[0].qty}"
                }
            }

        }

        containerView.cvOrderReadyToPay.setOnClickListener {
            data.let {
                listener?.onItemClick(it)
            }
        }
    }

    companion object {

        @JvmStatic
        fun create(viewGroup: ViewGroup): OrderReadyToPayViewHolder {
            return OrderReadyToPayViewHolder(viewGroup.inflate(R.layout.adapter_view_item_ready_to_pay))
        }

    }
}