package com.example.GinGapRao.ui.orders.payments

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.ascend.lib.promptpay.ThaiQRPromptPay
import com.ascend.lib.qr.BitmapUtil
import com.ascend.lib.qr.QrBitmapModel
import com.example.GinGapRao.R
import com.example.GinGapRao.data.model.CollectionOrderModel
import com.example.GinGapRao.printer.PrinterDemoActivity
import com.example.GinGapRao.viewmodel.OrdersMainViewModel
import kotlinx.android.synthetic.main.fragment_payment_via_qr.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.math.BigDecimal


class PaymentViaQRFragment : DialogFragment() {

    private var order: CollectionOrderModel? = null
    private val viewModel: OrdersMainViewModel by viewModel()
    private lateinit var mListener: OnItemOrderPaidClickListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.ThemeOverlay_MaterialComponents_Dialog)

        if (arguments != null) {
            order = arguments?.getParcelable(EXTRA_PAYMENT_VIA_QR_CODE)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_payment_via_qr, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initListener()
    }

    private fun initView() {
        var totalPrice = 0
        order?.items?.forEachIndexed { _, orderDetail ->
            totalPrice = (totalPrice + orderDetail.price!!).toInt()
        }

        txtTotalPriceOrders.text = "$totalPrice บาท"
        genMyQRPromptPay(totalPrice)

    }

    private fun initListener() {
        btnSuccessfulTransactionPayQR.setOnClickListener {
            dialogConfirmTransaction()
        }

    }

    private fun genMyQRPromptPay(price: Int) {
        val thaiQRPromptPay =
            ThaiQRPromptPay.Builder().dynamicQR().creditTransfer().mobileNumber("0849787267")
                .amount(BigDecimal(price)).build()

        val tag30 = thaiQRPromptPay.generateContent()
        val qrImage = QrBitmapModel(
            qrBitmap = BitmapUtil.create2DCoderBitmapWithOverlay(tag30, 500, 500, PrinterDemoActivity.getLogo(context as Context, "eat_with_us_2_red_120px.png")),
            barCodeBitmap = null)
        imgPromptPayMyQR.setImageBitmap(qrImage.qrBitmap)
    }

    private fun dialogConfirmTransaction(){
        AlertDialog.Builder(context as Context)
            .setIcon(R.drawable.ic_warning)
            .setTitle("คุณแน่ใจนะว่ายืนยันการทำรายการสำเร็จแล้ว?")
            .setMessage("ยืนยันการจ่ายเงินด้วย QR Code หากทำการจ่ายสำเร็จเรียบร้อยให้กดปุ่ม 'ยืนยัน' เพื่อเก็บการทำรายการนี้")
            .setPositiveButton("ยืนยัน"
            ) { _, _ ->
                viewModel.updateStatusOrder(order?.id!!, "PAID")
                dialog?.dismiss()
                delegatePaidClick()
            }
            .setNegativeButton("ยกเลิก"
            ) { _, _ ->
                // Close AlertDialog
            }
            .show()
    }

    fun setListener(listener: OnItemOrderPaidClickListener) {
        this.mListener = listener
    }

    private fun delegatePaidClick() {
        mListener.onItemPaidClick()
    }

    interface OnItemOrderPaidClickListener {
        fun onItemPaidClick()
    }

    companion object {
        const val TAG_NAME = "PaymentViaQRFragment"
        const val EXTRA_PAYMENT_VIA_QR_CODE = "EXTRA_PAYMENT_VIA_QR_CODE"

        fun newInstance(data: CollectionOrderModel?= null) = PaymentViaQRFragment()
            .apply {
                arguments = Bundle().apply {
                    putParcelable(EXTRA_PAYMENT_VIA_QR_CODE, data)
                }
            }
    }
}