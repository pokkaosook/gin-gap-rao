package com.example.GinGapRao.ui.orders.detail.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.GinGapRao.R
import com.example.GinGapRao.data.model.OrderDetail
import com.example.GinGapRao.extensions.gone
import com.example.GinGapRao.extensions.inflate
import com.example.GinGapRao.extensions.visible
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.adapter_view_item_detail_order.view.*

class OrderItemDetailViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
    LayoutContainer {

    fun bind(
        position: Int,
        data: OrderDetail,
        listener: OrderItemDetailAdapter.ItemDetailOrderListener?
    ) {
        data.let {
            containerView.txtNameOfItemOrder.text = "${position + 1}. ${data.name} X${data.qty}"
            containerView.txtPriceOfItemOrder.text = "${it.price}"

            if(it.orderStatus == "NONE" || it.orderStatus == "BILL"){
                containerView.checkBoxItemOrder.gone()
            }else {
                containerView.checkBoxItemOrder.visible()
                containerView.checkBoxItemOrder.isChecked = it.foodStatus == "Served"

            }

        }

        containerView.checkBoxItemOrder.setOnClickListener {
            data.let {
                if(containerView.checkBoxItemOrder.isChecked){
                    it.foodStatus = "Served"
                } else{
                    it.foodStatus = "None"
                }

                listener?.onItemClickCheckBox(it)
            }
        }
    }

    companion object {

        @JvmStatic
        fun create(viewGroup: ViewGroup): OrderItemDetailViewHolder {
            return OrderItemDetailViewHolder(viewGroup.inflate(R.layout.adapter_view_item_detail_order))
        }

    }
}