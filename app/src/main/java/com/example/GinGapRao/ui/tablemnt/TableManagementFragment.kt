package com.example.GinGapRao.ui.tablemnt

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.example.GinGapRao.R
import com.example.GinGapRao.data.model.TableModel
import com.example.GinGapRao.data.repository.TableRepositoryImpl
import com.example.GinGapRao.extensions.gone
import com.example.GinGapRao.extensions.observe
import com.example.GinGapRao.utils.GridSpacingItemDecoration
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.table_management_fragment.*


class TableManagementFragment : Fragment(), TableListAdapter.TableListener {

    private lateinit var tableMntViewModel: TableManagementViewModel
    private var tableListAdapter: TableListAdapter? = null
    private lateinit var viewModelFactory: TableManagementViewModel.TableMntViewModelFactory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.table_management_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initViewModel()
        initView()
        tableMntViewModel.fetchTables()
        observeData()

    }

    private fun initView() {
        addBtn.setOnClickListener {
            CreateTableDialogFragment.newInstance(tableMntViewModel)
                .show(childFragmentManager, "popup_info")
        }

        context.let {
            val spanCount = if(resources.getBoolean(R.bool.large_screen)) 3 else 2
                tableListAdapter =  TableListAdapter()
                tableListAdapter?.setListener(this@TableManagementFragment)
                list?.apply {
                    adapter = tableListAdapter
                    layoutManager = GridLayoutManager(context, spanCount)
                    addItemDecoration(GridSpacingItemDecoration(spanCount, 30, false))

                }
        }
    }

    private fun initViewModel() {
        viewModelFactory = TableManagementViewModel.TableMntViewModelFactory(TableRepositoryImpl())
        tableMntViewModel =
            ViewModelProvider(
                this@TableManagementFragment,
                viewModelFactory
            )
                .get(TableManagementViewModel::class.java)
    }

    private fun observeData() {
        observe(tableMntViewModel.tableLiveData){
            it?.let { list ->
                displayDynamicLayout(list)
            }
        }
    }

    private fun displayDynamicLayout(data: List<TableModel>) {
        context.let {
            if (data?.size ?: 0 > 0) {
                tableListAdapter?.addAllAndRefresh(data)
            } else {
                list?.gone()
            }
        }
    }

    companion object {
        val TAG = TableManagementFragment::class.java.simpleName
        fun newInstance() = TableManagementFragment()
    }

    override fun onItemClick(data: TableModel) {
        TableDetailDialogFragment.newInstance(data, tableMntViewModel).show(childFragmentManager, TableDetailDialogFragment.TAG)
    }

    override fun onItemLongClick(data: TableModel) {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle("ลบโต๊ะ")
            .setMessage("ต้องการลบโต๊ะ ${data.name}\nหรือไม่?")
            .setNegativeButton("ลบ", object : DialogInterface.OnClickListener{
                override fun onClick(dialog: DialogInterface?, which: Int) {
                    view
                }

            })
    }
}
