package com.example.GinGapRao.ui.orders.payments.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.GinGapRao.data.model.CollectionOrderModel

class OrderReadyToPayAdapter : RecyclerView.Adapter<OrderReadyToPayViewHolder>() {

    private val listMainMenu by lazy { mutableListOf<CollectionOrderModel>() }

    private var listener: OrderReadyToPayListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderReadyToPayViewHolder {
        return OrderReadyToPayViewHolder.create(parent)
    }

    override fun getItemCount(): Int {
        return listMainMenu.size
    }

    override fun onBindViewHolder(holder: OrderReadyToPayViewHolder, position: Int) {
        return holder.bind(listMainMenu[position], listener)
    }

    fun addAllAndRefresh(listMenu: List<CollectionOrderModel>) {
        if (listMainMenu.size > 0) listMainMenu.clear()
        listMainMenu.addAll(listMenu)
        notifyDataSetChanged()
    }

    fun setListener(listener: OrderReadyToPayListener) {
        this.listener = listener
    }

    interface OrderReadyToPayListener {
        fun onItemClick(data: CollectionOrderModel)
    }

}
