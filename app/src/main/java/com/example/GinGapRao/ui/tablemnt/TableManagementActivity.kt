package com.example.GinGapRao.ui.tablemnt

import android.os.Bundle
import com.example.GinGapRao.ui.base.BaseToolbarActivity

class TableManagementActivity : BaseToolbarActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_table_management)
        loadContentFragmentInfo()
        toolbar.title = "จัดการบริหารโต๊ะ"
    }

    private fun loadContentFragmentInfo() {
        replaceFragmentToContentContainer(TableManagementFragment.newInstance(), TableManagementFragment.TAG)
    }
}