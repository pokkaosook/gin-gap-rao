package com.example.GinGapRao.ui.orders.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class OrdersMainTabAdapter(fm: FragmentManager, private val listOf: List<Fragment>)
    : FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getPageTitle(position: Int): CharSequence? {

        return when(position) {
            0 -> "ออเดอร์"
            1 -> "พร้อมชำระเงิน"
            else -> null
        }
    }

    override fun getItem(position: Int): Fragment {
        return listOf[position]
    }

    override fun getCount(): Int {
        return listOf.size
    }

}
