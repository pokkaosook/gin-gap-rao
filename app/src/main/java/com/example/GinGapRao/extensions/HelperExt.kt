package com.example.GinGapRao.extensions

inline fun <T> lazyFast(crossinline func: () -> T): Lazy<T> = lazy(LazyThreadSafetyMode.NONE) {
    func()
}

inline fun consume(func: () -> Unit): Boolean {
    func()
    return true
}