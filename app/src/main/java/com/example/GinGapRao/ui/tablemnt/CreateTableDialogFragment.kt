package com.example.GinGapRao.ui.tablemnt

import android.os.Bundle
import android.view.*
import androidx.fragment.app.DialogFragment
import com.example.GinGapRao.R
import com.example.GinGapRao.data.model.CreateTableModel
import com.example.GinGapRao.data.model.TableModel
import com.example.GinGapRao.utils.FireStoreHelper.SHOP_NAME
import com.example.GinGapRao.utils.FireStoreHelper.TABLE_COLLECTION
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.dialog_create_table.*

class CreateTableDialogFragment: DialogFragment() {

    private lateinit var viewModel: TableManagementViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_create_table, container)
    }

    override fun onStart() {
        super.onStart()

        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.MATCH_PARENT
        dialog?.window?.setLayout(width, height)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog?.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT)
        dialog?.window?.setBackgroundDrawableResource(R.color.bg_popup_transparent)
        setStyle(DialogFragment.STYLE_NO_INPUT, android.R.style.Theme)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        createBtn.setOnClickListener {
            //Save
            val seat: Int = try {
                val seat = seats.editText?.text.toString()
                seat.toInt()
            } catch (ex: Exception) {
                0
            }
            val tableData = CreateTableModel(
                isIsOpen = false,
                name = tableName.editText?.text.toString().trim(),
                seats = seat,
                shopName = SHOP_NAME
            )

            viewModel.saveTable(tableData)
            dismiss()
        }

        closeBtn.setOnClickListener {
            dismiss()
        }
    }


    companion object{
        fun newInstance(tableMntViewModel: TableManagementViewModel): CreateTableDialogFragment{
            val dialog = CreateTableDialogFragment()
            dialog.setTableViewModel(tableMntViewModel)
            return dialog
        }
    }

    private fun setTableViewModel(tableMntViewModel: TableManagementViewModel) {
        this.viewModel = tableMntViewModel
    }
}