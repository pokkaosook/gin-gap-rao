package com.example.GinGapRao.printer

import android.app.Activity

class HardwareHelper : BaseHardwareHelper() {

    fun getHardwareDriver() = driverManager
}

interface HardwareHelperInterface {
    fun isDeviceSupportedPrintFeature(): Boolean
    fun saveHardwareInfo(activity: Activity?)
    fun initSdk()
    fun getSerialNo(): String?
    fun enablePrinttingLedStatus(enabled: Boolean)
    fun setPowerHardware(powerOn: Boolean)
}