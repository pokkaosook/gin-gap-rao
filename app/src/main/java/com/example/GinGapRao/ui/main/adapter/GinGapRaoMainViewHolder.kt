package com.example.GinGapRao.ui.main.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.GinGapRao.R
import com.example.GinGapRao.data.model.MainMenuItemModel
import com.example.GinGapRao.extensions.inflate
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.main_menu_img_with_bottom_text.view.*

class GinGapRaoMainViewHolder(override val containerView: View, private val viewType: Int) : RecyclerView.ViewHolder(containerView),
    LayoutContainer {

    fun bind(data: MainMenuItemModel, listener: GinGapRaoMainAdapter.ItemMainMenuListener?) {
        data.apply {
                when(entryItemName){
                    "Manager" -> {
                        containerView.imgItemMainMenu.setImageResource(R.drawable.ic_dinner_main)
                        containerView.txtItemMainMenu.text = "Table Management"
                    }
                    "Orders" -> {
                        containerView.imgItemMainMenu.setImageResource(R.drawable.ic_order_main)
                        containerView.txtItemMainMenu.text = "Manage orders"

                    }
                    "Printer" -> {
                        containerView.imgItemMainMenu.setImageResource(R.drawable.ic_printer)
                        containerView.txtItemMainMenu.text = "Printer"

                    }
                }

        }

        containerView.cvItemMainMenu.setOnClickListener {
            data.let {
                listener?.onItemClick(it)

            }
        }
    }

    companion object {

        @JvmStatic
        fun create(viewGroup: ViewGroup, viewType: Int): GinGapRaoMainViewHolder {
            return GinGapRaoMainViewHolder(viewGroup.inflate(R.layout.main_menu_img_with_bottom_text), viewType)
        }

    }
}