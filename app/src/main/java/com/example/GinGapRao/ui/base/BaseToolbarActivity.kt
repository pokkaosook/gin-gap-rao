package com.example.GinGapRao.ui.base

import android.os.Bundle
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.GinGapRao.R
import kotlinx.android.synthetic.main.activity_base_toolbar.*
import kotlinx.android.synthetic.main.view_base_toolbar.*

abstract class BaseToolbarActivity : AppCompatActivity() {

    val toolbar: Toolbar get() = baseToolbar

    val container: FrameLayout get() = baseContentContainer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base_toolbar)

        toolbar.setTitleTextAppearance(this, R.style.TrueTextAppearance)
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp)
        toolbar.setNavigationOnClickListener {
            finishOrPopBackStack(supportFragmentManager)
        }
    }

    open fun finishOrPopBackStack(fragmentManager: FragmentManager) {
        if (fragmentManager.backStackEntryCount > 0) {
            fragmentManager.popBackStack()
        } else {
            finish()
        }
    }

    fun replaceFragmentToContentContainer(fragment: Fragment, tag: String? = null) {
        supportFragmentManager.beginTransaction()
            .replace(container.id, fragment, tag)
            .commitAllowingStateLoss()
    }

    fun replaceFragmentToContentContainer(containerId: Int, fragment: Fragment, tag: String? = null) {
        supportFragmentManager.beginTransaction()
                .replace(containerId, fragment, tag)
                .commit()
        }

}