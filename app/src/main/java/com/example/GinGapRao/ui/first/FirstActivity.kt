package com.example.GinGapRao.ui.first

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.GinGapRao.R
import com.example.GinGapRao.data.model.CollectionOrderModel
import com.example.GinGapRao.data.model.OrderDetail
import com.example.GinGapRao.viewmodel.NewsViewModel
import com.google.firebase.FirebaseApp
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.Source
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.ext.getScopeId
import org.koin.ext.getScopeName

class FirstActivity : AppCompatActivity() {

    private val viewModel: NewsViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val db = Firebase.firestore

//        // Add FireStore
//        val city = CollectionOrderModel("1", "2020-08-17 12:15:00", "pending",
//            "1234", "1235", listOf(OrderDetail("หมูหันซ้าย","หมูหัน","120")))
//
//        db.collection("orders").document("Tiger").set(city)


        // Update FireStore
//        db.collection("orders").document("Tiger")
//            .update(
//                hashMapOf(
//                    "id" to "3",
//                    "status" to "done"
//                ) as Map<String, String>
//            )

        // Get FireStore
        db.collection("orders")
            .addSnapshotListener { snapshot, error ->
                if (snapshot != null && !snapshot.isEmpty) {
                        for (document in snapshot) {
                            Log.d("TAG66666666", "Current data: ${document.data}")
                        }
                    val taskList: List<CollectionOrderModel> = snapshot.toObjects(CollectionOrderModel::class.java)
                    Log.d("TAG88888888", "Current data: $taskList")

                } else {
                    Log.d("TAG77777777", "Current data: null")
                }

                if (error != null) {
                    Log.w("TAG55555555", "Listen failed.", error)
                    return@addSnapshotListener
                }
            }

//        db.collection("orders")
//        .get()
//            .addOnSuccessListener { querySnapshot ->
//                // Successfully received data. List in querySnapshot.documents
//                val taskList: List<RemoteTask> = querySnapshot.toObjects(RemoteTask::class.java)
//                for (document in querySnapshot) {
//                    Log.d("TAG111111", "${document.id} => ${document.data}")
//                }
//                Log.d("TAG111112", "${querySnapshot.documents.size}")
//            }
//            .addOnFailureListener { exception ->
//                // An error occurred while getting data
//            }

    }
}