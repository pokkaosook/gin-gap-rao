package com.example.GinGapRao.utils.dialog

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.example.GinGapRao.R
import com.example.GinGapRao.extensions.lazyFast

class DialogConfirmGetSlip(context: Context) {

    private val dialogView: View by lazyFast {
        LayoutInflater.from(context).inflate(R.layout.alert_dialog_img_and_button, null)
    }

    @PublishedApi
    internal val builder: AlertDialog.Builder = AlertDialog.Builder(context)
        .setView(dialogView)

    @PublishedApi
    internal val positiveButton: Button by lazyFast {
        dialogView.findViewById<Button>(R.id.btnConfirmGetSlip)
    }

    @PublishedApi
    internal val negativeButton: Button by lazyFast {
        dialogView.findViewById<Button>(R.id.btnNotGetSlip)
    }

    @PublishedApi
    internal val title: TextView by lazyFast {
        dialogView.findViewById<TextView>(R.id.txtTitle)
    }

    @PublishedApi
    internal val cancelDialog: ImageView by lazyFast {
        dialogView.findViewById<ImageView>(R.id.imgDismissDialog)
    }

    @PublishedApi
    internal var dialog: AlertDialog? = null

    var cancelable: Boolean = true

    inline fun positiveClick(crossinline func: () -> Unit = {}) {
        with(positiveButton) {
            setClickListenerToDialogButton(func)
        }
    }

    inline fun negativeClick(crossinline func: () -> Unit = {}) {
        with(negativeButton) {
            setClickListenerToDialogButton(func)
        }
    }

    inline fun cancelClick(crossinline func: () -> Unit = {}) {
        with(cancelDialog) {
            setClickListenerToDialogButton(func)
        }
    }

    fun create(): AlertDialog {
        dialog = builder
            .setCancelable(cancelable)
            .create()
        return dialog!!
    }

    private fun TextView.goneIfTextEmpty() {
        visibility = if (text.isNullOrEmpty()) {
            View.GONE
        } else {
            View.VISIBLE
        }
    }

    @PublishedApi
    internal inline fun Button.setClickListenerToDialogButton(crossinline func: () -> Unit = {}) {
        setOnClickListener {
            func()
            dialog?.dismiss()
        }
    }

    @PublishedApi
    internal inline fun ImageView.setClickListenerToDialogButton(crossinline func: () -> Unit = {}) {
        setOnClickListener {
            func()
            dialog?.dismiss()
        }
    }
}