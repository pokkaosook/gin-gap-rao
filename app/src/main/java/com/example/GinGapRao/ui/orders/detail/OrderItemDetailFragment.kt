package com.example.GinGapRao.ui.orders.detail

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.GinGapRao.R
import com.example.GinGapRao.data.model.CollectionOrderModel
import com.example.GinGapRao.data.model.OrderDetail
import com.example.GinGapRao.extensions.gone
import com.example.GinGapRao.extensions.lazyFast
import com.example.GinGapRao.extensions.observe
import com.example.GinGapRao.extensions.visible
import com.example.GinGapRao.ui.orders.detail.adapter.OrderItemDetailAdapter
import com.example.GinGapRao.utils.dateToString
import com.example.GinGapRao.utils.stringToDateFormat
import com.example.GinGapRao.viewmodel.OrdersMainViewModel
import kotlinx.android.synthetic.main.fragment_order_detail.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

class OrderItemDetailFragment : Fragment() {

    private val mDetailOrderAdapter by lazyFast { OrderItemDetailAdapter() }
    private var order: CollectionOrderModel? = null

    private val viewModel: OrdersMainViewModel by viewModel()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            order = arguments?.getParcelable(EXTRA_ORDER_CODE)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_order_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.setCurrentOrder(order)

       observe(viewModel.currentOrder){
           initView(it!!)
       }
    }

    private fun initView(it: CollectionOrderModel) {
        txtDetailNumberTable.text = order?.tableNo ?: ""
        txtNumberOrder.text = "สถานะทำรายการ : ออเดอร์"
        txtItemAmountOrder.text = "จำนวนทั้งหมด ${order?.items?.size} รายการ"

        val dataShow = stringToDateFormat(it.orderDate!!, Locale("th"))
        txtTimeOrder.text = "สั่งเมื่อ : ${dateToString(dataShow, Locale("th"))}"

        var totalPrice = 0
            it.items?.forEachIndexed { _, orderDetail ->
                totalPrice = (totalPrice + orderDetail.price!!).toInt()
        }

        textTotalPriceOfOrder.text = "$totalPrice"

        it.items?.forEachIndexed { _, orderDetail ->
           orderDetail.apply {
               orderStatus = it.status
               foodStatus = orderDetail.foodStatus
               description = orderDetail.description
               name = orderDetail.name
               price = orderDetail.price
               image = orderDetail.image
               qty = orderDetail.qty
           }
        }

        mDetailOrderAdapter.addAllAndRefresh(it.items!!)

        if (it.status == "IN_PROGRESS" || it.status == "DONE") {
            btnChangeStatusToInProgress.gone()
        }else {
            btnChangeStatusToInProgress.visible()
        }

        listOrdersDetail.apply {
            layoutManager = LinearLayoutManager(context as Context)
            setHasFixedSize(true)
            adapter = mDetailOrderAdapter
        }

        btnChangeStatusToInProgress.setOnClickListener { _ ->
            viewModel.updateStatusOrder(it.id!!, "IN_PROGRESS")
        }

        mDetailOrderAdapter.setListener(object : OrderItemDetailAdapter.ItemDetailOrderListener {
            override fun onItemClickCheckBox(data: OrderDetail) {
                var count = 0

                mDetailOrderAdapter.listMainMenu.forEachIndexed { index, orderDetail ->
                    if(orderDetail.foodStatus == "Served") {
                        count += 1
                    }
                }

                if(mDetailOrderAdapter.listMainMenu.size == count){
                    viewModel.updateStatusOrder(it.id!!, "DONE")
                } else {
                    viewModel.updateStatusOrder(it.id!!, "IN_PROGRESS")
                }

                viewModel.updateStatusDetailItemOrder(it.id!!, mDetailOrderAdapter.listMainMenu)
            }

        })

    }

    companion object {
        const val TAG_NAME = "OrderListDetailFragment"
        const val EXTRA_ORDER_CODE = "EXTRA_ORDER_CODE"

        fun newInstance(data: CollectionOrderModel?= null) = OrderItemDetailFragment()
            .apply {
            arguments = Bundle().apply {
                putParcelable(EXTRA_ORDER_CODE, data)
            }
        }
    }

}
