package com.example.GinGapRao.printer

import android.graphics.Bitmap
import android.os.AsyncTask
import android.util.Log
import com.ascend.lib.qr.BitmapUtil
import timber.log.Timber

class PrinterHelper (private val listener: PrinterListener) {

    //POS
    private val driverManager = HardwareHelper().getHardwareDriver()
    private var mIsPrinting = false
    private var mPrintStatus: Boolean = false
    private var mIsPrintSuccess: Boolean = false

    fun initPrinter() {
        driverManager.setPowerHardware(true)
    }

    fun closePrinter() {
        driverManager.setPowerHardware(false)
    }

    fun printQR(checkInTime: String, companyName: String, qrData: String?, logoBmp: Bitmap?, qrOverlay: Bitmap?) {
        PrintTask(checkInTime, companyName, qrData, logoBmp, qrOverlay).execute()
    }


     fun enablePrinttingLedStatus(enabled: Boolean) {
         driverManager.enablePrinttingLedStatus(enabled)
    }

    fun isHavePaper(): Boolean {
        var ret : Int = -1
        try {
            ret = driverManager.PrintInit(1, 16, 16, 0)
        } catch (e: Exception) {
            e.printStackTrace()
//                val initRet = e.exceptionCode
            Timber.i("initRer : $ret")
        }

        val status = driverManager.PrintCheckStatus()
        return  status != -1
    }

    private var RESULT_CODE = 0

    inner class PrintTask(
                          val checkInTime: String,
                          val companyName: String?,
                          val qrData: String?,
                          val logoBmp: Bitmap?,
                            val qrOverlay: Bitmap?) : AsyncTask<Void, Int, Boolean>() {

        override fun onPreExecute() {
            super.onPreExecute()
            listener.onStartPrint()
//            mView.showPrintingDialog()
        }

        override fun doInBackground(vararg params: Void?): Boolean {
            var ret = -1

            try {
                ret = driverManager.PrintInit(1, 16, 16, 0)
            } catch (e: Exception) {
                e.printStackTrace()
//                val initRet = e.exceptionCode
                Timber.i("initRer : $ret")
            }

            driverManager.PrintSetGray(5)

            ret = driverManager.PrintCheckStatus()
            publishProgress(ret)
            if (ret == -1) {
                RESULT_CODE = -1
                Timber.i("Error, No Paper ")
                return false
            } else if (ret == -2) {
                RESULT_CODE = -1
                Timber.i("Error, Printer Too Hot ")
                return false
            } else if (ret == -3) {
                RESULT_CODE = -1
                Timber.i("voltage = " /*+ BatteryV * 2*/)
                Timber.i("Battery less :" /*+ BatteryV * 2*/)
                return false
            } else if (ret == -1002) {
                driverManager.setPowerHardware(true)
                return false
            } else {
                RESULT_CODE = 0
            }

            mIsPrinting = true

            try {
                enablePrinttingLedStatus(true)
                val nl = "\r\n"
                val verticalLine = "_____________________________"
                val subTitle = "กิน กับ เรา\nทดสอบถาษาไทย กี่ ซ้ำ ทำ อะไร เพื่อนี่ซี่ กุ้งย่าง"

                val dateTimeCheckin = "Check-in time    : $checkInTime"
                val sign = "Sign__________________________"

                if(logoBmp != null) {
                    driverManager.setAlign(1)
                    driverManager.PrintBmp(logoBmp)
                    driverManager.PrintStr("")
                }
                driverManager.PrintStr("")
                driverManager.setAlign(0)
                driverManager.PrintStr(companyName)



                val printContent = StringBuilder()
                printContent.append("Parking paper").append(nl)
                    .append(nl)
                    .append(subTitle)

                    .append(dateTimeCheckin).append(nl)
                    .append(nl)
//                        .append(nl)
//                        .append(nl)
                    .append(verticalLine).append(nl)
                    .append(nl)
                    .append(nl)
                    .append(nl)
                    .append(sign).append(nl)
                    .append(nl)

                driverManager.PrintStr(printContent.toString())
                driverManager.PrintStr(" ")


                    val qrCodeBmp = BitmapUtil.create2DCoderBitmapWithOverlay(qrData, 300, 300, qrOverlay)
                driverManager.setAlign(1)
                driverManager.PrintBmp(qrCodeBmp)
                driverManager.setAlign(0)

                driverManager.PrintStr(" ")
                driverManager.PrintStr(" ")
                driverManager.PrintStr(" ")
                driverManager.PrintStr(" ")
                driverManager.PrintStr(" ")
                driverManager.PrintStr(" ")
                driverManager.PrintStr(" ")
                ret = driverManager.PrintStart()
                publishProgress(ret)

                if (logoBmp?.isRecycled == false) {
                    logoBmp?.recycle()
                }

                if (ret != 0) {
                    RESULT_CODE = -1
                    Log.e("liuhao", "Lib_PrnStart fail, ret = $ret")
                    if (ret == -1) {
                        Timber.i("No Print Paper ")
//                        mView.showPrinterErrorDialog(R.string.printer_out_of_paper)
                    } else if (ret == -2) {
                        Timber.i("too hot ")
                    } else if (ret == -3) {
                        Timber.i("low voltage ")
                    } else {
                        Timber.i("Print fail ")
                    }
                    mIsPrintSuccess = false
                } else {
                    mIsPrintSuccess = true

                    RESULT_CODE = 0
                    Timber.i("Print Finish ")
                }

            } catch (ex: Exception) {
                mIsPrintSuccess = false
                publishProgress(1)
                ex.printStackTrace()
            } finally {
                enablePrinttingLedStatus(false)
            }

            Log.i("Performance", "End Print")
            return mIsPrintSuccess
        }

        override fun onProgressUpdate(vararg values: Int?) {
            super.onProgressUpdate(*values)

            when (values[0]) {
                -1 -> {
                    RESULT_CODE = -1
                    Timber.i("Error, No Paper ")
                    listener.onError(PrinterErrorCode.NO_PAPER)
//                    mView.showPrinterErrorDialog(R.string.printer_out_of_paper)
                }
                -2 -> {
                    RESULT_CODE = -1
                    Timber.i("Error, Printer Too Hot ")
                    listener.onError(PrinterErrorCode.PRINTER_TOO_HOT)
//                    mView.showPrinterErrorDialog(R.string.printer_too_hot)
                }
                -3 -> {
                    RESULT_CODE = -1
                    Timber.i("voltage = " /*+ BatteryV * 2*/)
                    Timber.i("Battery less :" /*+ BatteryV * 2*/)
                    listener.onError(PrinterErrorCode.LOW_BATTERY)
//                    mView.showPrinterErrorDialog(R.string.printer_low_battery)
                }
                -1002 -> {
                    driverManager.setPowerHardware(true)
                    listener.onError(PrinterErrorCode.NOT_SET_POWER_TO_PRINTER)
                }
                else -> {
                    RESULT_CODE = 0
                }
            }
        }


        override fun onPostExecute(result: Boolean) {
            super.onPostExecute(result)
//            mView.showPrintFinish()
            listener.onPrintFinished(result)
            mIsPrinting = false
        }
    }

    interface PrinterListener{
        fun onStartPrint()
        fun onPrintFinished(success: Boolean)
        fun onError(errorCode: PrinterErrorCode)
    }

    enum class PrinterErrorCode {
        NO_PAPER ,
        PRINTER_TOO_HOT,
        LOW_BATTERY,
        NOT_SET_POWER_TO_PRINTER,
        NO_ERROR
    }
}