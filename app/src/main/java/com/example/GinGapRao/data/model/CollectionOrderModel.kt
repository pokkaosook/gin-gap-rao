package com.example.GinGapRao.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CollectionOrderModel(
    var id: String? = null,
    val orderDate: String? = null,
    var status: String? = null,
    val tableNo: String? = null,
    val transection: String? = null,
    var active: Boolean = false,
    val items: List<OrderDetail>? = null
) : Parcelable

@Parcelize
data class OrderDetail(
    var foodStatus: String? = null,
    var orderStatus: String? = null,
    var description: String? = null,
    var name: String? = null,
    var price: Long? = null,
    var image: String? = null,
    var qty: Int? = null
) : Parcelable
