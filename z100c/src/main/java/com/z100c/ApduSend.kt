package com.z100c

import vpos.apipackage.APDU_SEND

class ApduSend(command: ByteArray, Lc: Short, dataIn: ByteArray, Le: Short) {

    private val apdu = APDU_SEND(command, Lc, dataIn, Le)

    fun sendBytes(): ByteArray {
        return apdu.bytes
    }
}