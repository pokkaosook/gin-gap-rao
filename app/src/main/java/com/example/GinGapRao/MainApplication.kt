package com.example.GinGapRao

import android.app.Application
import android.util.Log
import com.example.GinGapRao.data.model.Device
import com.example.GinGapRao.data.model.NotificationsModel
import com.example.GinGapRao.di.module.networkModule
import com.example.GinGapRao.di.module.newsModule
import com.example.GinGapRao.utils.Contextor
import com.example.GinGapRao.utils.FireStoreHelper
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseApp
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import timber.log.Timber

class MainApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
        FirebaseApp.initializeApp(this)

        Contextor.context = this

        startKoin {
            androidLogger()
            androidContext(this@MainApplication)
            modules(listOf(
                networkModule,
                newsModule
            ))
        }

        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w("FFB", "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                val token = task.result?.token


                Log.d("FFB", token)



                token?.let {
                    val db = Firebase.firestore
                    db.collection("merchantNotifications")
                        .whereEqualTo("shopId", FireStoreHelper.SHOP_ID)
                        .limit(1)
                        .get()
                        .addOnSuccessListener {
                            val notificationsModelList =
                                it.toObjects(NotificationsModel::class.java)
                            if (notificationsModelList.size > 0) {
                                val document = it.documents.get(0)
                                val notiModel = notificationsModelList.get(0)
                                var devices: ArrayList<Device>? = notiModel.devices
                                val newDevices = devices?.filter { device ->
                                    device.notiToken == token
                                }
                                if (newDevices.isNullOrEmpty()) {
                                    if(devices.isNullOrEmpty()) {
                                        devices = arrayListOf<Device>()
                                        notiModel.devices = devices
                                    }
                                    val device = createDevice(token)
                                    devices.add(device)

                                } else {
                                    newDevices.get(0).notiToken = token
                                }

                                db.collection("merchantNotifications")
                                    .document(document.id)
                                    .set(notiModel)
                                    .addOnSuccessListener {
                                        Log.i("FCM", "Update success")
                                    }

                            } else {
                                val devices = arrayListOf<Device>()
                                val device = createDevice(token)
                                devices.add(device)
                                val noti = NotificationsModel(
                                    shopId = FireStoreHelper.SHOP_ID,
                                    shopName = FireStoreHelper.SHOP_NAME,
                                    devices = devices
                                )

                                db.collection("merchantNotifications")
                                    .document()
                                    .set(noti)
                                    .addOnSuccessListener {
                                        Log.i("FCM", "Add new success")
                                    }

                            }
                        }
                }
            })

        FirebaseMessaging.getInstance().isAutoInitEnabled = true
    }

    private fun createDevice(token: String?): Device {
        val device = Device(
            deviceName = "",
            notiToken = token
        )
        return device
    }
}