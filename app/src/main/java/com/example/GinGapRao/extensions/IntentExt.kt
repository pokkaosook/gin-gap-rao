package com.example.GinGapRao.extensions

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import androidx.fragment.app.Fragment

inline fun <reified T : Activity> Context.openActivity() {
    openActivity<T> { }
}

inline fun <reified T : Activity> Context.openActivity(block: Intent.() -> Unit) {
    val intent = Intent(this, T::class.java)
    intent.block()
    startActivity(intent)
}

inline fun <reified T : Activity> Activity.openActivityForResult(requestCode: Int) {
    openActivityForResult<T>(requestCode) { }
}

inline fun <reified T : Activity> Activity.openActivityForResult(requestCode: Int, block: Intent.() -> Unit) {
    val intent = Intent(this, T::class.java)
    intent.block()
    startActivityForResult(intent, requestCode)
}

inline fun <reified T : Activity> Fragment.openActivity() {
    activity?.openActivity<T>()
}

inline fun <reified T : Activity> Fragment.openActivity(block: Intent.() -> Unit) {
    activity?.openActivity<T> { block() }
}

inline fun <reified T : Activity> Fragment.openActivityForResult(requestCode: Int) {
    openActivityForResult<T>(requestCode) { }
}

inline fun <reified T : Activity> Fragment.openActivityForResult(requestCode: Int, block: Intent.() -> Unit) {
    val intent = Intent(context, T::class.java)
    intent.block()
    startActivityForResult(intent, requestCode)
}

inline fun Activity.finishWithResult(resultCode: Int, block: Intent.() -> Unit) {
    val intent = Intent()
    intent.block()
    setResult(resultCode, intent)
    finish()
}

fun Intent.clearTasks() {
    addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
}

fun Intent.clearTop() {
    addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
}

fun openAppSettings(context: Context) {
    val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).apply {
        data = Uri.parse("package:" + context.packageName)
        addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
    }

    context.startActivity(intent)
}

fun openUriIntent(context: Context, uri: Uri) {
    val intent = Intent(Intent.ACTION_VIEW)
    intent.data = uri
    context.startActivity(intent)
}