package com.example.GinGapRao.utils

import java.text.SimpleDateFormat
import java.util.*

fun dateToString(date: Date, locale: Locale): String {
    return try {
        val dateFormat = "HH : mm น."
        val outputFormat = SimpleDateFormat(dateFormat, locale)
        val outputText = outputFormat.format(date)
        println(outputText)
        outputText
    } catch (er: Exception) {
        ""
    }
}

fun stringToDateFormat(inputText: String,locale: Locale): Date {
    return try {
        val inputFormat = SimpleDateFormat("yyyy-MM-dd' 'hh:mm:ss", locale)
        inputFormat.timeZone = TimeZone.getTimeZone("Asia/Bangkok ")
        val date = inputFormat.parse(inputText)

        if(locale.language == "th"){
            date.year = date.year+543
        }

        date.hours = date.hours-7
        date
    } catch (er: Exception) {
        Date()
    }
}