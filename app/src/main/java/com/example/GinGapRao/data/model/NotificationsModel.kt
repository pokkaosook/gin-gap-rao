package com.example.GinGapRao.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NotificationsModel(
    var devices: ArrayList<Device>? = null,
    var shopId: String? = null,
    var shopName: String? = null
): Parcelable


@Parcelize
data class Device(
    var deviceName: String? = null,
    var notiToken: String? = null
): Parcelable