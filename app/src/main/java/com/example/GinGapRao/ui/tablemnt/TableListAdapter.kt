package com.example.GinGapRao.ui.tablemnt

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.GinGapRao.data.model.TableModel

class TableListAdapter : RecyclerView.Adapter<TableListViewHolder>() {

    private val listMainMenu by lazy { mutableListOf<TableModel>() }

    private var listener: TableListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TableListViewHolder {
        return TableListViewHolder.create(parent)
    }

    override fun getItemCount(): Int {
        return listMainMenu.size
    }

    override fun onBindViewHolder(holder: TableListViewHolder, position: Int) {
        return holder.bind(listMainMenu[position], listener)
    }

    fun addAllAndRefresh(listMenu: List<TableModel>) {
        if(listMainMenu.size > 0) listMainMenu.clear()
        listMainMenu.addAll(listMenu)
        notifyDataSetChanged()
    }

    fun setListener(listener: TableListener) {
        this.listener = listener
    }

    interface TableListener {
        fun onItemClick(data: TableModel)
        fun onItemLongClick(data: TableModel)
    }

}