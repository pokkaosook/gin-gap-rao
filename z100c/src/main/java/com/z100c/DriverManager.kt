package com.z100c

import android.graphics.Bitmap
import com.google.zxing.BarcodeFormat
import vpos.apipackage.PosApiHelper
import vpos.apipackage.Print
import vpos.apipackage.Sys

class DriverManager{
    private var posApiHelper: PosApiHelper = PosApiHelper.getInstance()

    fun setLed(ledIndex: Int, enabled: Int) {
        posApiHelper.SysSetLedMode(ledIndex, enabled)
    }

    fun closeIcc(slot: Byte): Int {
        return posApiHelper.IccClose(slot)
    }

    fun iccCommand(slot: Byte, apduSend: ByteArray?, apduResp: ByteArray?): Int {
        return posApiHelper.IccCommand(slot, apduSend, apduResp)
    }

    fun iccCheck(slot: Byte): Int {
        return posApiHelper.IccCheck(slot)
    }

    fun iccClose(slot: Byte): Int {
        return posApiHelper.IccClose(slot)
    }

    fun iccOpen(type: Byte, vccMode: Byte, atr: ByteArray): Int {
        return posApiHelper.IccOpen(type, vccMode, atr)
    }

    fun mcrOpen(): Int {
        return posApiHelper.McrOpen()
    }

    fun mcrReset(): Int {
        return posApiHelper.McrReset()
    }

    fun mcrCheck(): Int {
        return posApiHelper.McrCheck()
    }

    fun mcrRead(keyNo: Byte, mode: Byte, track1: ByteArray?, track2: ByteArray?, track3: ByteArray?): Int {
        return posApiHelper.McrRead(keyNo, mode, track1, track2, track3)
    }

    fun sysBeep(): Int {
        return posApiHelper.SysBeep()
    }

    fun mcrClose(): Int {
        return posApiHelper.McrClose()
    }

    fun PrintInit(): Int {
        return posApiHelper.PrintInit()
    }

    fun PrintInit(gray: Int, fontHeight: Int, fontWidth: Int, fontZoom: Int): Int{
        return posApiHelper.PrintInit(gray, fontHeight, fontWidth, fontZoom)
    }

    /**
     * @param grayLevel value 1 - 5
     * <br/> 1:Lowest    3：medium  5：Highest
     * @return 0 –successfully, Other	-failure
     */
    fun PrintSetGray(grayLevel: Int): Int {
        return posApiHelper.PrintSetGray(grayLevel)
    }

    fun PrintCheckStatus(): Int {
        return posApiHelper.PrintCheckStatus()
    }

    fun PrintSetFont(fontHeight: Byte, fontWidth: Byte, zoomLevel: Byte): Int {
        return posApiHelper.PrintSetFont(fontHeight, fontWidth, zoomLevel)
    }

    fun PrintSetFont(fontSize: Int): Int {
        return posApiHelper.PrintSetFont(fontSize.toByte(), fontSize.toByte(), 0x00.toByte())
    }

    fun PrintStr(value: String?): Int {
        return posApiHelper.PrintStr(value)
    }

    fun PrintQRBarcode(content: String?, width: Int, height: Int): Int {
        return posApiHelper.PrintBarcode(content, width, height, BarcodeFormat.QR_CODE)
    }

    fun PrintCtnStart(): Int {
        return posApiHelper.PrintCtnStart()
    }

    fun PrintBmp(bitmap: Bitmap?): Int {
        return posApiHelper.PrintBmp(bitmap)
    }

    fun PrintStart(): Int {
        return posApiHelper.PrintStart()
    }

    /**
     * @param 0 = Left, 1 = center
     */
    fun setAlign(align: Int){
        Print.Lib_PrnSetAlign(align)
    }



    companion object{
        const val LED_Blue = 1
        const val LED_Yellow = 2
        const val LED_Green = 3
        const val LED_Red = 4

        var INSTANCE: DriverManager? = null
        fun getInstance(): DriverManager {
            if(INSTANCE == null){
                INSTANCE = DriverManager()
            }
            return INSTANCE!!
        }
    }

    fun setPowerHardware(powerOn: Boolean) {
        Sys.Lib_Setpower(if(powerOn) 1 else 0);
    }

    fun enablePrinttingLedStatus(enabled: Boolean) {
        setLed(LED_Yellow, if(enabled) 1 else 0)
    }

}