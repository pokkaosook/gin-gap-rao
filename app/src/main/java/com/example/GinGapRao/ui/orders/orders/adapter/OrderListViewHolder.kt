package com.example.GinGapRao.ui.orders.orders.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.GinGapRao.R
import com.example.GinGapRao.data.model.CollectionOrderModel
import com.example.GinGapRao.extensions.gone
import com.example.GinGapRao.extensions.inflate
import com.example.GinGapRao.extensions.visible
import com.example.GinGapRao.utils.Contextor.context
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.adapter_view_item_order.view.*

class OrderListViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
    LayoutContainer {

    fun bind(data: CollectionOrderModel, listener: OrderListAdapter.ItemOrderListener?) {
        data.let {

            containerView.txtNumberTable.text = it.tableNo ?: ""
            containerView.txtCountItemOrder.text = "${it.items?.size} รายการ"

            when {
                it.items!!.size > 3 -> {
                    containerView.txtFirstItemOrder.visible()
                    containerView.txtSecondItemOrder.visible()
                    containerView.txtThirdItemOrder.visible()

                    containerView.txtFirstItemOrder.text = "${it.items[0].name} X${it.items[0].qty}"
                    containerView.txtSecondItemOrder.text = "${it.items[1].name} X${it.items[1].qty}"
                    containerView.txtThirdItemOrder.text = "${it.items[2].name} X${it.items[2].qty}"

                }
                it.items.size == 2 -> {
                    containerView.txtFirstItemOrder.visible()
                    containerView.txtSecondItemOrder.visible()
                    containerView.txtThirdItemOrder.gone()

                    containerView.txtFirstItemOrder.text = "${it.items[0].name} X${it.items[0].qty}"
                    containerView.txtSecondItemOrder.text = "${it.items[1].name} X${it.items[1].qty}"
                }
                else -> {
                    containerView.txtFirstItemOrder.visible()
                    containerView.txtSecondItemOrder.gone()
                    containerView.txtThirdItemOrder.gone()

                    containerView.txtFirstItemOrder.text = "${it.items[0].name} X${it.items[0].qty}"
                }
            }

            when (it.status) {
                "IN_PROGRESS" -> {
                    containerView.constStatusBarOrder.visible()
                    containerView.txtStatusOrder.text = "กำลังปรุง"
                    containerView.constStatusBarOrder.background =
                        ContextCompat.getDrawable(context, R.drawable.bg_status_order_cooking)
                }
                "DONE" -> {
                    containerView.constStatusBarOrder.visible()
                    containerView.txtStatusOrder.text = "ปรุงเสร็จ"
                    containerView.constStatusBarOrder.background =
                        ContextCompat.getDrawable(context as Context, R.drawable.bg_status_order_done_cook)
                }
                else -> {
                    containerView.constStatusBarOrder.gone()
                }
            }

        }

        containerView.cvItemOrder.setOnClickListener {
            data.let {
                listener?.onItemClick(it)
            }
        }
    }

    companion object {

        @JvmStatic
        fun create(viewGroup: ViewGroup): OrderListViewHolder {
            return OrderListViewHolder(viewGroup.inflate(R.layout.adapter_view_item_order))
        }

    }
}