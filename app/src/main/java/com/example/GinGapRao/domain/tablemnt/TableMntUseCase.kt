package com.example.GinGapRao.domain.tablemnt

import com.example.GinGapRao.data.model.CreateTableModel
import com.example.GinGapRao.data.model.TableModel
import com.example.GinGapRao.data.repository.TableRepository
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class TableMntUseCase(private val tableRepository: TableRepository) {

    suspend fun getTable(getTableRequestParam: GetTableRequestParam): List<TableModel>?{
        return tableRepository.getTableList(getTableRequestParam)
    }

    suspend fun createTable(model: CreateTableModel){
        return tableRepository.createTable(model)
    }

    data class GetTableRequestParam(
        val shopId: String?)
}