package com.example.GinGapRao.extensions

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat

fun Context.getColorCompat(@ColorRes colorRes: Int) = ContextCompat.getColor(this, colorRes)

fun Context.getDrawableCompat(@DrawableRes drawableRes: Int) = ContextCompat.getDrawable(this, drawableRes)

fun Context.getDrawableFromName(drawbleString: String): Drawable? {
    val drawableRes = this.resources.getIdentifier(drawbleString, "drawable", this.packageName)
    return ContextCompat.getDrawable(this, drawableRes)
}