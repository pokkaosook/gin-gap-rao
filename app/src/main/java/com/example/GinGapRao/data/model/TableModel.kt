package com.example.GinGapRao.data.model

import android.os.Parcelable
import com.google.firebase.firestore.IgnoreExtraProperties
import kotlinx.android.parcel.Parcelize

@Parcelize
@IgnoreExtraProperties
data class TableModel(
    var id: String? = null,
    var name: String? = null,
    var seats: Int = 0,
    var isIsOpen: Boolean = false
): Parcelable

@Parcelize
@IgnoreExtraProperties
data class CreateTableModel(
    var name: String? = null,
    var seats: Int = 0,
    var `isIsOpen`: Boolean = false,
    var shopName: String? = null,
    var shopId: String? = "11111"
): Parcelable