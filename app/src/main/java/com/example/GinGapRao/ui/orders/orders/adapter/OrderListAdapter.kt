package com.example.GinGapRao.ui.orders.orders.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.GinGapRao.data.model.CollectionOrderModel

class OrderListAdapter : RecyclerView.Adapter<OrderListViewHolder>() {

    private val listMainMenu by lazy { mutableListOf<CollectionOrderModel>() }

    private var listener: ItemOrderListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderListViewHolder {
        return OrderListViewHolder.create(parent)
    }

    override fun getItemCount(): Int {
        return listMainMenu.size
    }

    override fun onBindViewHolder(holder: OrderListViewHolder, position: Int) {
        return holder.bind(listMainMenu[position], listener)
    }

    fun addAllAndRefresh(listMenu: List<CollectionOrderModel>) {
        if(listMainMenu.size > 0) listMainMenu.clear()
        listMainMenu.addAll(listMenu)
        notifyDataSetChanged()
    }

    fun setListener(listener: ItemOrderListener) {
        this.listener = listener
    }

    interface ItemOrderListener {
        fun onItemClick(data: CollectionOrderModel)
    }

}