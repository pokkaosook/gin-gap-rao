package com.example.GinGapRao.ui.main.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.GinGapRao.data.model.MainMenuItemModel

class GinGapRaoMainAdapter : RecyclerView.Adapter<GinGapRaoMainViewHolder>() {

    private val listMainMenu by lazy { mutableListOf<MainMenuItemModel>() }

    private var listener: ItemMainMenuListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GinGapRaoMainViewHolder {
        return GinGapRaoMainViewHolder.create(parent, viewType)
    }

    override fun getItemCount(): Int {
        return listMainMenu.size
    }

    override fun onBindViewHolder(holder: GinGapRaoMainViewHolder, position: Int) {
        return holder.bind(listMainMenu[position], listener)
    }

    fun addAllAndRefresh(listMenu: List<MainMenuItemModel>) {
        if(listMainMenu.size > 0) listMainMenu.clear()
        listMainMenu.addAll(listMenu)
        notifyDataSetChanged()
    }

    fun setListener(listener: ItemMainMenuListener) {
        this.listener = listener
    }

    interface ItemMainMenuListener {
        fun onItemClick(data: MainMenuItemModel)
    }

}