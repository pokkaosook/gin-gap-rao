package com.example.GinGapRao.ui.orders.payments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.GinGapRao.R
import com.example.GinGapRao.data.model.CollectionOrderModel
import com.example.GinGapRao.extensions.*
import com.example.GinGapRao.ui.orders.payments.adapter.OrderReadyToPayAdapter
import com.example.GinGapRao.viewmodel.OrdersReadyToPayViewModel
import kotlinx.android.synthetic.main.fragment_order_ready_to_pay.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class OrderReadyToPayFragment : Fragment() {

    private val mOrderReadyToPayAdapter by lazyFast { OrderReadyToPayAdapter() }
    private val viewModel: OrdersReadyToPayViewModel by viewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_order_ready_to_pay, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeData()
        initListener()

        viewModel.getOrderReadyToPayFireStore()
    }

    private fun observeData() {
        observe(viewModel.orderReadyToPayList){
            if (it != null && it.isNotEmpty()) {
                initView(it)
                listOrdersReadyToPay.visible()
                contWaitingPayment.gone()
            } else {
                listOrdersReadyToPay.gone()
                contWaitingPayment.visible()
            }
        }
    }

    private fun initView(it: List<CollectionOrderModel>) {
        mOrderReadyToPayAdapter.addAllAndRefresh(it)
        listOrdersReadyToPay.apply {
            layoutManager = LinearLayoutManager(context as Context)
            setHasFixedSize(true)
            adapter = mOrderReadyToPayAdapter
        }
    }

    private fun initListener() {
        mOrderReadyToPayAdapter.setListener(object : OrderReadyToPayAdapter.OrderReadyToPayListener {
            override fun onItemClick(data: CollectionOrderModel) {
                delegateOnReadyToPayClick(data)
            }

        })
    }

    private fun delegateOnReadyToPayClick(data: CollectionOrderModel) {
        delegateTo<OnItemOrderReadyToPayClickListener> {
            it.onItemReadyToPayClick(data)
        }
    }

    interface OnItemOrderReadyToPayClickListener {
        fun onItemReadyToPayClick(data: CollectionOrderModel)
    }

    companion object {

        @JvmStatic
        fun newInstance() = OrderReadyToPayFragment()
    }

}