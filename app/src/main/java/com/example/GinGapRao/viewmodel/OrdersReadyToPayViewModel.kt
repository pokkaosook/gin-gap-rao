package com.example.GinGapRao.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.GinGapRao.data.model.CollectionOrderModel
import com.example.GinGapRao.data.model.OrderDetail
import com.example.GinGapRao.data.model.PrinterModel
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase

class OrdersReadyToPayViewModel() : ViewModel()  {

    val orderReadyToPayList = MutableLiveData<List<CollectionOrderModel>>()

    fun getOrderReadyToPayFireStore(){

        val db = Firebase.firestore

        db.collection("orders")
            .whereEqualTo("status","BILL")
            .addSnapshotListener { snapshot, error ->
                if (snapshot != null && !snapshot.isEmpty) {

                    val list = arrayListOf<CollectionOrderModel>()
                    snapshot.documents.forEachIndexed{index, documentSnapshot ->
                        val temp = documentSnapshot.toObject<CollectionOrderModel>()
                        temp?.id = documentSnapshot.id
                        if (temp != null) {
                            list.add(temp)
                        }
                    }

//                    val taskList: List<CollectionOrderModel> = snapshot.toObjects(CollectionOrderModel::class.java)

                    orderReadyToPayList.value = list.toList()

                } else {
                    val taskList: List<CollectionOrderModel> = listOf()
                    orderReadyToPayList.value = taskList
                }


                if (error != null) {
                    return@addSnapshotListener
                }
            }
    }

    fun updateStatusDetailItemOrder(
        data: CollectionOrderModel?
    ){
        data?.status = "PAID"

        val db = Firebase.firestore
        db.collection("orders").document(data?.id!!)
            .update(
                hashMapOf(
                    "status" to data.status
                ) as Map<String, String>
            )

    }

    fun orderPrinter(order: CollectionOrderModel?) {
        val print = PrinterModel(
            order?.id,
            "11111",
            "new",
            order?.tableNo
        )

        val db = Firebase.firestore
        db.collection("printer").document().set(print)
    }

    class OrderViewModelFactory()
        : ViewModelProvider.Factory{

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if(modelClass.isAssignableFrom(OrdersReadyToPayViewModel::class.java)){
                return  OrdersReadyToPayViewModel() as T
            }
            throw IllegalArgumentException("Unknown ViewModel Class")
        }
    }

}