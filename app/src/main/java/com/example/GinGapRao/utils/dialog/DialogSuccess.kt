package com.example.GinGapRao.utils.dialog

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.example.GinGapRao.R
import com.example.GinGapRao.extensions.lazyFast

class DialogSuccess(context: Context) {

    private val dialogView: View by lazyFast {
        LayoutInflater.from(context).inflate(R.layout.dialog_fragment_success, null)
    }

    @PublishedApi
    internal val builder: AlertDialog.Builder = AlertDialog.Builder(context)
        .setView(dialogView)


    @PublishedApi
    internal var dialog: AlertDialog? = null

    var cancelable: Boolean = true

    fun create(): AlertDialog {
        dialog = builder
            .setCancelable(cancelable)
            .create()
        return dialog!!
    }

    private fun TextView.goneIfTextEmpty() {
        visibility = if (text.isNullOrEmpty()) {
            View.GONE
        } else {
            View.VISIBLE
        }
    }

}
