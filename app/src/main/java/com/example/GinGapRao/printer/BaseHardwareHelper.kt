package com.example.GinGapRao.printer

import android.app.Activity
import com.z100c.DriverManager

open class BaseHardwareHelper : HardwareHelperInterface {
    companion object{
        const val TAG = "BaseHardwareHelperZ100"
    }

    var driverManager: DriverManager = DriverManager.getInstance()

    override fun isDeviceSupportedPrintFeature(): Boolean {
        return try {
            driverManager.setPowerHardware(true)
            true
        }catch (ex: Exception){
            false
        }
    }

    override fun saveHardwareInfo(activity: Activity?) {
    }

    override fun initSdk() {
    }

    override fun getSerialNo(): String? {
        return ""
    }

    override fun enablePrinttingLedStatus(enabled: Boolean) {

    }

    override fun setPowerHardware(powerOn: Boolean) {

    }


}