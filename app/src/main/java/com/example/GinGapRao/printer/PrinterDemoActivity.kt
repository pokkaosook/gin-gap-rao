package com.example.GinGapRao.printer

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.ascend.lib.promptpay.PromptPayAccountType
import com.ascend.lib.promptpay.PromptPayHelper
import com.ascend.lib.promptpay.ThaiQRPromptPay
import com.ascend.lib.qr.BitmapUtil
import com.ascend.lib.qr.QrBitmapModel
import com.example.GinGapRao.R
import kotlinx.android.synthetic.main.activity_printer_demo.*
import java.io.File
import java.io.IOException
import java.io.InputStream
import java.math.BigDecimal

class PrinterDemoActivity : AppCompatActivity() {

    lateinit var printer : PrinterHelper
    lateinit var hardwareHelper: HardwareHelper
    var isSupportPrinter: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_printer_demo)

        try {
            hardwareHelper = HardwareHelper()
            isSupportPrinter = true
        }catch (e: java.lang.Exception){
            e.printStackTrace()
            isSupportPrinter = false
        }

        if(isSupportPrinter) {
            printer = PrinterHelper(object : PrinterHelper.PrinterListener {
                override fun onStartPrint() {

                }

                override fun onPrintFinished(success: Boolean) {

                }

                override fun onError(errorCode: PrinterHelper.PrinterErrorCode) {

                }

            })
        }

        printBtn.setOnClickListener {
            val tag29 = PromptPayHelper.generateTag29Data(
                PromptPayAccountType.MOBILE_PHONE_NUMBER,
                "0849787267",
                "350.99")

            val thaiQRPromptPay =
                ThaiQRPromptPay.Builder().dynamicQR().creditTransfer().mobileNumber("0849787267")
                    .amount(BigDecimal("1.0")).build()

            val tag30 = thaiQRPromptPay.generateContent()
            Log.i("PromptPay", "Tag29: $tag29")
            Log.i("PromptPay", "Tag30: $tag30")
            val qrImage = QrBitmapModel(
                qrBitmap = BitmapUtil.create2DCoderBitmapWithOverlay(tag30, 500, 500, getLogo(this@PrinterDemoActivity, "eat_with_us_2_red_180px.png")),
                barCodeBitmap = null)
            qrImageView.setImageBitmap(qrImage.qrBitmap)
//            qrImageView.setImageBitmap(qrImage.barCodeBitmap)


            if(isSupportPrinter && hardwareHelper.isDeviceSupportedPrintFeature()) {
                val havePaper = printer.isHavePaper()
                if(!havePaper) {
                    Toast.makeText(this@PrinterDemoActivity, "Out of PAPER!!", Toast.LENGTH_LONG).show()
                }else {
                    printer.printQR(
                        "12:50น.",
                        "ร้่านก๋วยเตี๋ยวไก่",
                        tag30,
                        getLogo(this@PrinterDemoActivity, LOGO_FILE_NAME),
                        getLogo(this@PrinterDemoActivity, "eat_with_us_white.png")
                    )
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        printer.initPrinter()
    }

    override fun onStop() {
        super.onStop()
        printer.closePrinter()
    }



/*
    @SuppressLint("CheckResult")
    fun createRedeemBitmap(code: String, cardViewPoint: View, height: Int, width: Int) {
        Single.just(createBitmapForQRAndBarcode(code))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    redeemBitMap.value = it
                    getScreenShort(cardViewPoint, height, width)
                }, {
                    redeemBitMap.value = null
                })
    }

    @SuppressLint("CheckResult")
    fun getScreenShort(cardViewPoint: View, height: Int, width: Int) {
        Single.just(capture(cardViewPoint, height, width))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    screenShortStatus.value = true
                },
                {
                    screenShortStatus.value = false
                }
            )
    }
*/



    companion object {
        const val LOGO_FILE_NAME = "eat_wit_us_black_100px.png"


        fun getLogo(context: Context, fileName: String = LOGO_FILE_NAME): Bitmap? {
            try {
                val dir = context.filesDir
                if (dir.exists()) {
                    val logoFile = File(dir, LOGO_FILE_NAME)
                    if (logoFile.exists()) {
                        val inputStream: InputStream = context.openFileInput(LOGO_FILE_NAME)
                        val d = Drawable.createFromStream(inputStream, null)
                        return (d as BitmapDrawable).bitmap
                    }
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
            try {
                val inputStream = context.assets.open(fileName)
                val d = Drawable.createFromStream(inputStream, null)
                return (d as BitmapDrawable).bitmap
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return null
        }
    }
}