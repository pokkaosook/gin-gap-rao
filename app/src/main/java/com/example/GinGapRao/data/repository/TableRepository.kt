package com.example.GinGapRao.data.repository

import com.example.GinGapRao.data.model.CreateTableModel
import com.example.GinGapRao.data.model.TableModel
import com.example.GinGapRao.domain.tablemnt.TableMntUseCase
import com.example.GinGapRao.utils.FireStoreHelper
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.tasks.await

interface TableRepository{

    suspend fun getTableList(
        tableRequestParam: TableMntUseCase.GetTableRequestParam
    ): List<TableModel>?

    suspend fun createTable(model: CreateTableModel)
    suspend fun updateTable(tableModel: TableModel)
}

class TableRepositoryImpl(): TableRepository {

    override suspend fun getTableList(
        tableRequestParam: TableMntUseCase.GetTableRequestParam): List<TableModel>? {
        val db = Firebase.firestore
        val list = arrayListOf<TableModel>()

       db.collection("tables")
            .whereEqualTo("shopId", tableRequestParam.shopId)
            .addSnapshotListener {value, e ->
                if (e != null) {
                    return@addSnapshotListener
                }
                for (doc in value!!) {
                    val temp = doc.toObject<TableModel>()
                    temp?.let {
                        it.id = doc.id
                        list.add(it)
                    }
                }
            }
        return list
    }

    override suspend fun createTable(model: CreateTableModel) {
        val db = Firebase.firestore
        val result = db.collection(FireStoreHelper.TABLE_COLLECTION).document()
            .set(model)
            .await()

        return
    }

    override suspend fun updateTable(tableModel: TableModel) {
        val db = Firebase.firestore
        val result = db.collection(FireStoreHelper.TABLE_COLLECTION).document(tableModel?.id ?: "")
            .update("isOpen", tableModel.isIsOpen)
            .await()

        return
    }
}