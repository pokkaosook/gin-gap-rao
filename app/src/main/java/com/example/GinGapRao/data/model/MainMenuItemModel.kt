package com.example.GinGapRao.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MainMenuItemModel(
    var entryItemName : String? = null
) : Parcelable