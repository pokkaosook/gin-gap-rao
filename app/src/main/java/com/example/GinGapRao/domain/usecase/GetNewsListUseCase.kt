package com.example.GinGapRao.domain.usecase

import com.example.GinGapRao.data.repository.NewsRepository
import com.example.GinGapRao.data.response.Article
import com.example.GinGapRao.domain.base.BaseUseCase
import io.reactivex.Single

class GetNewsListUseCase constructor(
    private val repository: NewsRepository
): BaseUseCase<Any, List<Article>>() {

    override fun buildUseCaseObservable(): Single<List<Article>> {
        return repository.getNewsApi()
            .onErrorResumeNext {
                Single.error(it)
            }
    }
}
