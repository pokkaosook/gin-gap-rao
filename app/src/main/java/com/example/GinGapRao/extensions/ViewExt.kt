package com.example.GinGapRao.extensions

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Typeface
import android.os.Build
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.core.widget.NestedScrollView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.tabs.TabLayout

val ViewGroup.children: List<View>
    get() = List(childCount) { getChildAt(it) }

fun View.visible() {

    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun View.gone() {
    visibility = View.GONE
}

fun ImageView.disableImageView() {
    isEnabled = false
    alpha = 0.3f
}

fun ImageView.enableImageView() {
    isEnabled = true
    alpha = 1.0f
}

fun View.visibleWhen(condition: Boolean) {
    if (condition) {
        visible()
    } else {
        gone()
    }
}

fun View.goneWhen(condition: Boolean) {
    if (condition) {
        gone()
    } else {
        visible()
    }
}

fun View.isVisible() = visibility == View.VISIBLE
fun View.isInvisible() = visibility == View.INVISIBLE
fun View.isGone() = visibility == View.GONE

inline fun Context.getStyledAttributes(
    set: AttributeSet?,
    attrs: IntArray,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0,
    func: TypedArray.() -> Unit
) {
    if (set == null) {
        return
    }

    val typedArray: TypedArray = obtainStyledAttributes(set, attrs, defStyleAttr, defStyleRes)
    try {
        typedArray.func()
    } finally {
        typedArray.recycle()
    }
}

fun ViewGroup.inflate(resource: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(resource, this, attachToRoot)
}

fun NestedScrollView.focusOnView(view: View) {
    this.post { this.scrollTo(0, view.top) }
}

fun View.convertViewToBitmap(contWidth: Int, contHeight: Int): Bitmap? {
    val setBitmap = Bitmap.createBitmap(contWidth, contHeight, Bitmap.Config.ARGB_8888)
    val canvasView = Canvas(setBitmap)

    val drawableView = this.background
    if (drawableView != null)
        drawableView.draw(canvasView)
    else
        canvasView.drawColor(Color.TRANSPARENT)

    this.draw(canvasView)
    return setBitmap
}

fun showHideBottomSheet(sheetBehavior: BottomSheetBehavior<FrameLayout>?, isShow: Boolean) {
    if (isShow) {
        sheetBehavior?.state = BottomSheetBehavior.STATE_EXPANDED
    } else {
        sheetBehavior?.state = BottomSheetBehavior.STATE_COLLAPSED
    }
}


/**
 * An extension which performs an [action] if the view has been measured, otherwise waits for it to
 * be measured, and then performs the [action].
 */
inline fun View.measured(crossinline action: () -> Unit) {
    if (Build.VERSION.SDK_INT >= 19 && isLaidOut) {
        action()
        return
    }

    addOnLayoutChangeListener(object : View.OnLayoutChangeListener {
        override fun onLayoutChange(
            view: View,
            left: Int,
            top: Int,
            right: Int,
            bottom: Int,
            oldLeft: Int,
            oldTop: Int,
            oldRight: Int,
            oldBottom: Int
        ) {
            removeOnLayoutChangeListener(this)
            action()
        }
    })
}

fun TabLayout.applyFont(typeface: Typeface) {
    val viewGroup = getChildAt(0) as ViewGroup
    val tabsCount = viewGroup.childCount
    for (j in 0 until tabsCount) {
        val viewGroupChildAt = viewGroup.getChildAt(j) as ViewGroup
        val tabChildCount = viewGroupChildAt.childCount
        for (i in 0 until tabChildCount) {
            val tabViewChild = viewGroupChildAt.getChildAt(i)
            if (tabViewChild is TextView) {
                tabViewChild.typeface = typeface
            }
        }
    }
}
