package com.example.GinGapRao.ui.tablemnt

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.GinGapRao.data.model.CreateTableModel
import com.example.GinGapRao.data.model.TableModel
import com.example.GinGapRao.data.repository.TableRepository
import com.example.GinGapRao.domain.tablemnt.TableMntUseCase
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class TableManagementViewModel(val tableRepository: TableRepository)
    : ViewModel(), CoroutineScope {
    val TAG = TableManagementViewModel::class.java.simpleName

    val shopId =  "11111"
    val tableLiveData = MutableLiveData<List<TableModel>>()

    private val job = Job()
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main


    fun fetchTables(){
        getTableList( TableMntUseCase.GetTableRequestParam(shopId = shopId))
    }

     fun getTableList(
        tableRequestParam: TableMntUseCase.GetTableRequestParam) {
         val db = Firebase.firestore

        db.collection("tables")
            .whereEqualTo("shopId", tableRequestParam.shopId)
            .addSnapshotListener {value, e ->
                if (e != null) {
                    return@addSnapshotListener
                }
                val list = arrayListOf<TableModel>()

                for (doc in value!!) {
                    val temp = doc.toObject<TableModel>()
                    temp?.let {
                        it.id = doc.id
                        list.add(it)
                    }
                }
                tableLiveData.value = list
            }
    }

    fun saveTable(tableData: CreateTableModel) {
        launch {
            tableRepository.createTable(tableData)
        }
    }

    fun updateTable(tableModel: TableModel) {
        launch {
            tableRepository.updateTable(tableModel)
        }
    }


    class TableMntViewModelFactory(private val tableRepository: TableRepository)
        : ViewModelProvider.Factory{

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if(modelClass.isAssignableFrom(TableManagementViewModel::class.java)){
                return  TableManagementViewModel(tableRepository) as T
            }
            throw IllegalArgumentException("Unknown ViewModel Class")
        }
    }

}