package com.example.GinGapRao.ui.tablemnt

import android.os.Bundle
import android.view.*
import androidx.fragment.app.DialogFragment
import com.ascend.lib.qr.BitmapUtil
import com.example.GinGapRao.R
import com.example.GinGapRao.data.model.TableModel
import com.example.GinGapRao.utils.FireStoreHelper
import kotlinx.android.synthetic.main.dialog_table_detail.*

class TableDetailDialogFragment: DialogFragment() {
    var tableModel: TableModel? = null
    private lateinit var viewModel: TableManagementViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_table_detail, container)
    }

    override fun onStart() {
        super.onStart()

        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.MATCH_PARENT
        dialog?.window?.setLayout(width, height)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog?.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT)
        dialog?.window?.setBackgroundDrawableResource(R.color.bg_popup_transparent)
        setStyle(DialogFragment.STYLE_NO_INPUT, android.R.style.Theme)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        showData(tableModel)
    }

    private fun showData(tableModel: TableModel?) {
        tableModel?.let {
            tableStatusToggle.isChecked = it.isIsOpen
            val qrContent = QR_PREFIX + it.id
            val qrImage = BitmapUtil.createQRCode(qrContent, 300)
            qr.setImageBitmap(qrImage)
            title.text = getString(R.string.table_detail_dialog_title, it.name)
            shopName.text = getString(R.string.table_detail_dialog_shop_name, FireStoreHelper.SHOP_NAME)
            tableSeat.text = getString(R.string.table_detail_dialog_table_seat, it.seats)

            tableStatusToggle.setOnClickListener {
                val isChecked = tableStatusToggle.isChecked
                tableModel.isIsOpen = isChecked
                viewModel.updateTable(tableModel)

                tableStatusTxt.text = getString(R.string.table_detail_status, if(isChecked) "เปิดใช้" else "ว่าง")
            }
        }
    }

    private fun setData(tableModel: TableModel) {
        this.tableModel = tableModel
    }

    private fun setTableViewModel(tableMntViewModel: TableManagementViewModel) {
        this.viewModel = tableMntViewModel
    }

    companion object {
        const val TAG = "TableDetailDialogFragment"
        const val QR_PREFIX = "https://gin-gap-rao.web.app?table="

        fun newInstance(
            tableModel: TableModel,
            tableMntViewModel: TableManagementViewModel
        ): TableDetailDialogFragment{
            val fragment = TableDetailDialogFragment()
            fragment.setData(tableModel)
            fragment.setTableViewModel(tableMntViewModel)
            return fragment
        }
    }
}