package com.example.GinGapRao.ui.main

import android.os.Bundle
import com.example.GinGapRao.R
import com.example.GinGapRao.ui.base.BaseToolbarActivity
import com.google.firebase.auth.FirebaseAuth

class GinGapRaoMainActivity : BaseToolbarActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        toolbar.title = getString(R.string.app_name)
        toolbar.navigationIcon = null
        loadContentFragmentInfo()
//
//        var mAuth = FirebaseAuth.getInstance()
//        mAuth.signInWithEmailAndPassword("gin-gap-rao@ascendcorp.com", "gin-gap-rao")
//            .addOnCompleteListener(this@GinGapRaoMainActivity) { task ->
//                if (task.isSuccessful) {
//                    // Sign in success, update UI with the signed-in user's information
//                    loadContentFragmentInfo()
//                } else {
//                    // If sign in fails, display a message to the user.
////                    view?.hideLoader()
////                    failure(task)
//                }
//            }
    }

    private fun loadContentFragmentInfo() {
        replaceFragmentToContentContainer(GinGapRaoMainFragment(), GinGapRaoMainFragment.TAG_NAME)
    }


}