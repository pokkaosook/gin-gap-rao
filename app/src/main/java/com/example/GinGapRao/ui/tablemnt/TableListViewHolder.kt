package com.example.GinGapRao.ui.tablemnt

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.GinGapRao.R
import com.example.GinGapRao.data.model.TableModel
import com.example.GinGapRao.extensions.inflate
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_table_list.view.*

class TableListViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
    LayoutContainer {

    fun bind(data: TableModel, listener: TableListAdapter.TableListener?) {
        data.let {
            var statusText: String = ""
            if(it.isIsOpen){
                statusText = "ถูกใช้งาน"
                containerView.status.background = containerView.context.getDrawable(R.drawable.bg_chip_active)
            }else{
                statusText = "ว่าง"
                containerView.status.background = containerView.context.getDrawable(R.drawable.bg_chip_disable)

            }
            containerView.tableName.text = containerView.context.getString(R.string.table_detail_dialog_table_name, it.name)
            containerView.tableSeat.text = containerView.context.getString(R.string.table_detail_dialog_table_seat, it.seats)
            containerView.status.text = statusText
        }

        containerView.setOnClickListener {
            data.let {
                listener?.onItemClick(it)
            }
        }

        containerView.setOnLongClickListener {
            data.let {
                listener?.onItemLongClick(it)
            }
            false
        }
    }

    companion object {

        @JvmStatic
        fun create(viewGroup: ViewGroup): TableListViewHolder {
            return TableListViewHolder(viewGroup.inflate(R.layout.item_table_list))
        }

    }
}